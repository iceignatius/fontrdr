#ifndef _FONT_RENDERER_FLAGS_H_
#define _FONT_RENDERER_FLAGS_H_

#ifdef __cplusplus
extern "C" {
#endif

enum fontrdr_flags
{
    FONTRDR_REGULAR     = 0,
    FONTRDR_BOLD        = 0x40000000,
    FONTRDR_OBLIQUE     = 0x20000000,
    FONTRDR_MONOSPACE   = 0x10000000,
    FONTRDR_UNDERLINE   = 0x08000000,

    FONTRDR_FLAGSMASK   = 0x7F000000,
    FONTRDR_STYLEMASK   = 0x60000000,
    FONTRDR_SIZEMASK    = 0x0000FFFF,
};

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
