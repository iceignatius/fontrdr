#ifndef _FONT_RENDERER_H_
#define _FONT_RENDERER_H_

#include <uimg/uimg.h>
#include "flags.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct fontrdr fontrdr_t;

fontrdr_t* fontrdr_create(void);
fontrdr_t* fontrdr_shadow(fontrdr_t *self);
void fontrdr_release(fontrdr_t *self);

int fontrdr_set_cache_num(fontrdr_t *self, unsigned num);
void fontrdr_clear_cache(fontrdr_t *self);
int fontrdr_add_fonts(fontrdr_t *self, const char *wildcard);
void fontrdr_clear_fonts(fontrdr_t *self);

uimg_t* fontrdr_gen_glyph(
    fontrdr_t *self, uint32_t code, uint32_t colour, unsigned size, int flags);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
