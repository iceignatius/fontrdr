## Source Codes Modified From

The simple list container:
<https://people.freebsd.org/~anchie/send-net-mgmt/send03_ports/ports/net-mgmt/send/work/send_0.3/include/list.h>

BSD source codes:
<https://github.com/freebsd/freebsd-src/tree/master>

Tree module documents:
<https://www.freebsd.org/cgi/man.cgi?query=tree&sektion=3&manpath=freebsd-release-ports>

Files are modified from the source tree (commit: de1aa3d) of:
* sys/sys/tree.h
* sys/compat/linuxkpi/common/include/linux/rbtree.h
* sys/compat/linuxkpi/common/include/linux/list.h

## Known Problems

* Warning the "dereferencing type-punned pointer will break strict-aliasing rules":

    The warning occurred when using GCC with optimisation (-fstrict-aliasing).
    To solve this problem, one of the following methods can be used:

    1. Add `-fno-strict-aliasing` compile option to
        disable the strict aliasing behaviour,
        or

    2. Add the following pre-compile options between
        the `RB_GENERATE` or `RB_GENERATE_STATIC` statement:

            #pragma GCC push_options
            #pragma GCC optimize("no-strict-aliasing")

            RB_GENERATE(...);

            #pragma GCC pop_options
