cmake_minimum_required(VERSION 3.5)
project(fontrdr)

if(LIB_FREETYPE)
    set(srcs ${srcs} ${PROJECT_SOURCE_DIR}/fontrdr_file_freetype.c)
endif()
set(srcs ${srcs} ${PROJECT_SOURCE_DIR}/fontrdr_cache.c)
set(srcs ${srcs} ${PROJECT_SOURCE_DIR}/fontrdr.c)

if(LIB_FREETYPE)
    include_directories(/usr/include/freetype2)
endif()
include_directories(${CMAKE_SOURCE_DIR}/include)

set(depend_libs ${depend_libs} ${LIB_UIMG})
if(LIB_FREETYPE)
    set(depend_libs ${depend_libs} ${LIB_FREETYPE})
    set(depend_libs ${depend_libs} "-pthread")
endif()

if(CMAKE_COMPILER_IS_GNUCC)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pthread")
endif()
if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
endif()

add_library(${PROJECT_NAME} ${srcs})
target_link_libraries(${PROJECT_NAME} ${depend_libs})

install(TARGETS ${PROJECT_NAME} DESTINATION lib)
install(DIRECTORY ${CMAKE_SOURCE_DIR}/include/ DESTINATION include/${PROJECT_NAME})
