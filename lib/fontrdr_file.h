#ifndef _FONT_RENDERER_FILE_H_
#define _FONT_RENDERER_FILE_H_

#include <uimg/uimg.h>
#include "flags.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef uimg_t greyimg_t;
typedef struct fontrdr_file fontrdr_file_t;

fontrdr_file_t* fontrdr_file_open(const char *filename);
void fontrdr_file_close(fontrdr_file_t *self);

const char* fontrdr_file_get_name(const fontrdr_file_t *self);
int fontrdr_file_get_style(const fontrdr_file_t *self);

/*
 * Below functions may not be functional before setting a font size!
 * Except that the font is a fixed size font and not scalable.
 */
int fontrdr_file_set_size(fontrdr_file_t *self, int h);
int fontrdr_file_get_size(fontrdr_file_t *self);
int fontrdr_file_get_uline_y(fontrdr_file_t *self);
int fontrdr_file_get_uline_thickness(fontrdr_file_t *self);
int fontrdr_file_get_advance(fontrdr_file_t *self, uint32_t code);

/*
 * Generate a grey scale glyph image, fixed hight but variable width
 */
greyimg_t* fontrdr_file_render(fontrdr_file_t *self, uint32_t code);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
