#ifndef _FONT_RENDERER_CACHE_H_
#define _FONT_RENDERER_CACHE_H_

#include <uimg/uimg.h>
#include "bsd/list.h"
#include "bsd/tree.h"

#ifdef __cplusplus
extern "C" {
#endif

struct fontrdr_cnode
{
    // Private node reference
    struct list_head list_node;
    RB_ENTRY(fontrdr_cnode) map_node;

    // key
    uint32_t code;
    uint32_t colour;
    unsigned size_flags;

    // value
    uimg_t *img;
};

int fontrdr_cnode_compare(
    const struct fontrdr_cnode *l, const struct fontrdr_cnode *r);

RB_HEAD(fontrdr_cmap, fontrdr_cnode);

typedef struct fontrdr_cache
{
    unsigned node_capa;
    unsigned node_num;
    struct fontrdr_cnode *node_pool;

    struct list_head free_list;
    struct list_head using_list;
    struct fontrdr_cmap glyph_map;
} fontrdr_cache_t;

void fontrdr_cache_init(fontrdr_cache_t *self);
void fontrdr_cache_destroy(fontrdr_cache_t *self);

int fontrdr_cache_alloc(fontrdr_cache_t *self, unsigned num);
void fontrdr_cache_dealloc(fontrdr_cache_t *self);

const struct fontrdr_cnode* fontrdr_cache_add(
    fontrdr_cache_t *self,
    uint32_t code,
    uint32_t colour,
    unsigned size,
    int flags,
    const uimg_t *img);
const struct fontrdr_cnode* fontrdr_cache_find(
    fontrdr_cache_t *self,
    uint32_t code,
    uint32_t colour,
    unsigned size,
    int flags);
void fontrdr_cache_clear(fontrdr_cache_t *self);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
