#include <assert.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <math.h>
#include <ft2build.h>
#include "fontrdr_file.h"
#include FT_FREETYPE_H

// Convert fixed point 26.6 value to an integer value.
#define FIXPT266_TO_INT(val) \
    ( (int) round( ( (val) >> 6 ) + ( (val) & 0x3F )/( (double)0x40 ) ) )

static FT_Library lib_inst = NULL;
static atomic_int lib_refcnt = ATOMIC_VAR_INIT(0);

struct fontrdr_file
{
    FT_Face face;
    char *name;
    int style;

    int height;
    int bline_y;
    int uline_y;
    int uline_d;
};

static
int init_ft_library(void)
{
    if( 0 != atomic_fetch_add(&lib_refcnt, 1) )
        return 0;

    if(FT_Init_FreeType(&lib_inst))
    {
        atomic_fetch_sub(&lib_refcnt, 1);
        return -1;
    }

    return 0;
}

static
void destroy_ft_library(void)
{
    if( 1 != atomic_fetch_sub(&lib_refcnt, 1) )
        return;

    if(lib_inst)
        FT_Done_FreeType(lib_inst);
}

fontrdr_file_t* fontrdr_file_open(const char *filename)
{
    fontrdr_file_t *self = NULL;

    bool succ = false;
    do
    {
        if( !filename || !filename[0] )
            break;

        if(init_ft_library())
            break;

        if(!( self = calloc(1, sizeof(*self)) ))
            break;

        if(FT_New_Face(lib_inst, filename, 0, &self->face))
            break;

        if(!( self->name = strdup(self->face->family_name) ))
            break;

        self->style = 0;
        if( self->face->style_flags & FT_STYLE_FLAG_BOLD )
            self->style |= FONTRDR_BOLD;
        if( self->face->style_flags & FT_STYLE_FLAG_ITALIC )
            self->style |= FONTRDR_OBLIQUE;

        self->height = -1;
        self->bline_y = -1;
        self->uline_y = -1;
        self->uline_d = -1;

        succ = true;
    } while(false);

    if( !succ && self )
    {
        fontrdr_file_close(self);
        self = NULL;
    }

    return self;
}

void fontrdr_file_close(fontrdr_file_t *self)
{
    if(self->name)
        free(self->name);
    if(self->face)
        FT_Done_Face(self->face);
    free(self);

    destroy_ft_library();
}

const char* fontrdr_file_get_name(const fontrdr_file_t *self)
{
    return self->name;
}

int fontrdr_file_get_style(const fontrdr_file_t *self)
{
    return self->style;
}

int fontrdr_file_set_size(fontrdr_file_t *self, int h)
{
    if( h <= 0 )
        return -1;
    if( self->height == h )
        return 0;

    FT_Face face = self->face;
    FT_Error err = FT_Set_Pixel_Sizes(face, 0, h);

    /*
     * Update parameters by current setting if change size failed,
     * or by new setting if success.
     */

    self->height = face->size->metrics.y_ppem;
    assert( self->height > 0 );

    int ascender = abs(face->ascender);
    int descender = abs(face->descender);
    double em_h = ascender + descender;

    self->bline_y = round( self->height * ascender / em_h );
    if( self->bline_y < 0 )
        self->bline_y = 0;
    if( self->bline_y >= self->height )
        self->bline_y = self->height - 1;

    int uline_asc = ascender - face->underline_position;
    self->uline_y = round( self->height * uline_asc / em_h );
    if( self->uline_y < 0 )
        self->uline_y = 0;
    if( self->uline_y >= self->height )
        self->uline_y = self->height - 1;

    self->uline_d = round( self->height * face->underline_thickness / em_h );
    if( self->uline_d <= 0 )
        self->uline_d = 1;
    else if( self->uline_d > self->height )
        self->uline_d = self->height;

    if( self->uline_y + self->uline_d > self->height )
        self->uline_y = self->height - self->uline_d;

    return err;
}

int fontrdr_file_get_size(fontrdr_file_t *self)
{
    return self->height;
}

int fontrdr_file_get_uline_y(fontrdr_file_t *self)
{
    return self->uline_y;
}

int fontrdr_file_get_uline_thickness(fontrdr_file_t *self)
{
    return self->uline_d;
}

int fontrdr_file_get_advance(fontrdr_file_t *self, uint32_t code)
{
    FT_Face face = self->face;

    unsigned glyidx = FT_Get_Char_Index(face, code);
    if(!glyidx)
        return 0;

    if(FT_Load_Glyph(face, glyidx, 0))
        return 0;

    if(FT_Render_Glyph(face->glyph, FT_RENDER_MODE_MONO))
        return 0;

    return FIXPT266_TO_INT(face->glyph->metrics.horiAdvance);
}

static
void blit_glyph(greyimg_t *img, int bline_y, const FT_GlyphSlot glyph)
{
    uimg_fill(img, 0);

    int img_w = uimg_get_width(img);
    int img_h = uimg_get_height(img);
    int gly_left = glyph->bitmap_left;
    int gly_top = bline_y - glyph->bitmap_top;
    int gly_w = glyph->bitmap.width;
    int gly_h = glyph->bitmap.rows;
    int gly_pitch = abs(glyph->bitmap.pitch);

    // Try to adjust glyph position if it is out of range (vertical)
    if( gly_h > img_h )
        gly_top = -( ( gly_h - img_h ) >> 1 );
    else if( gly_top < 0 )
        gly_top = 0;
    else if( gly_top + gly_h > img_h )
        gly_top = img_h - gly_h;

    // Try to adjust glyph position if it is out of range (horizontal)
    if( gly_w > img_w )
        gly_left = -( ( gly_w - img_w ) >> 1 );
    else if( gly_left < 0 )
        gly_left = 0;
    else if( gly_left + gly_w > img_w )
        gly_left = img_w - gly_w;

    uimg_begin_buf_access(img);

    for(int gly_y = 0; gly_y < gly_h; ++gly_y)
    {
        int img_y = gly_top + gly_y;
        if( img_y < 0 || img_h <= img_y )
            continue;

        const uint8_t *gly_row = glyph->bitmap.buffer + gly_y * gly_pitch;
        uint32_t *img_row = uimg_get_row_buf(img, img_y);

        for(int gly_x = 0; gly_x < gly_w; ++gly_x)
        {
            int img_x = gly_left + gly_x;
            if( img_x < 0 || img_w <= img_x )
                continue;

            img_row[img_x] = gly_row[gly_x];
        }
    }

    uimg_end_buf_access(img);
}

greyimg_t* fontrdr_file_render(fontrdr_file_t *self, uint32_t code)
{
    greyimg_t *img = NULL;

    do
    {
        FT_Face face = self->face;

        unsigned glyidx = FT_Get_Char_Index(face, code);
        if(!glyidx)
            break;

        if(FT_Load_Glyph(face, glyidx, 0))
            break;

        if(FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL))
            break;

        int width = FIXPT266_TO_INT(face->glyph->metrics.horiAdvance);
        if( width <= 0 )
            break;

        if(!( img = uimg_create(width, self->height) ))
            break;

        blit_glyph(img, self->bline_y, face->glyph);

    } while(false);

    return img;
}
