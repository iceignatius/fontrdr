#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <threads.h>
#include <glob.h>
#include "minmax.h"
#include "fontrdr_file.h"
#include "fontrdr_cache.h"
#include "fontrdr.h"

struct fontrdr
{
    atomic_int refcnt;
    mtx_t mutex;

    fontrdr_file_t **fontlist;
    unsigned fontnum;

    fontrdr_cache_t cache;

    int height;
    int space_width;
    int ascii_width;
};

fontrdr_t* fontrdr_create(void)
{
    fontrdr_t *self = malloc(sizeof(*self));
    if(!self)
        return NULL;

    atomic_init(&self->refcnt, 1);
    mtx_init(&self->mutex, mtx_recursive);

    self->fontlist = NULL;
    self->fontnum = 0;

    fontrdr_cache_init(&self->cache);

    self->height = 0;
    self->space_width = 0;
    self->ascii_width = 0;

    return self;
}

fontrdr_t* fontrdr_shadow(fontrdr_t *self)
{
    atomic_fetch_add(&self->refcnt, 1);
    return self;
}

void fontrdr_release(fontrdr_t *self)
{
    int prev_refcnt = atomic_fetch_sub(&self->refcnt, 1);
    assert( prev_refcnt > 0 );
    if( prev_refcnt != 1 )
        return;

    fontrdr_cache_destroy(&self->cache);
    fontrdr_clear_fonts(self);
    fontrdr_clear_cache(self);
    mtx_destroy(&self->mutex);
    free(self);
}

int fontrdr_set_cache_num(fontrdr_t *self, unsigned num)
{
    return fontrdr_cache_alloc(&self->cache, num);
}

void fontrdr_clear_cache(fontrdr_t *self)
{
    fontrdr_cache_clear(&self->cache);
}

static
int add_single_font(fontrdr_t *self, const char *filename)
{
    fontrdr_file_t *newfile = NULL;

    int err = 0;
    do
    {
        if(!( newfile = fontrdr_file_open(filename) ))
        {
            err = EBFONT;
            break;
        }

        fontrdr_file_t **newlist = realloc(
            self->fontlist, ( self->fontnum + 1 ) * sizeof(self->fontlist[0]));
        if(!newlist)
        {
            err = ENOMEM;
            break;
        }

        self->fontlist = newlist;
        self->fontlist[self->fontnum++] = newfile;
        newfile = NULL;

    } while(false);

    if(newfile)
        fontrdr_file_close(newfile);

    return err;
}

int fontrdr_add_fonts(fontrdr_t *self, const char *wildcard)
{
    glob_t info = {0};
    int err = glob(wildcard, 0, NULL, &info);
    if(err)
        return err == GLOB_NOSPACE ? ENOMEM : EINVAL;

    mtx_lock(&self->mutex);

    unsigned count = 0;
    for(size_t i = 0; i < info.gl_pathc; ++i)
    {
        err = add_single_font(self, info.gl_pathv[i]);
        if(!err)
        {
            ++count;
            self->height = 0;
            self->space_width = 0;
            self->ascii_width = 0;
        }
    }

    mtx_unlock(&self->mutex);

    globfree(&info);

    return count ? 0 : err;
}

void fontrdr_clear_fonts(fontrdr_t *self)
{
    mtx_lock(&self->mutex);

    for(unsigned i = 0; i < self->fontnum; ++i)
        fontrdr_file_close(self->fontlist[i]);

    if(self->fontlist)
        free(self->fontlist);

    self->fontlist = NULL;
    self->fontnum = 0;
    self->height = 0;
    self->space_width = 0;
    self->ascii_width = 0;

    fontrdr_clear_cache(self);

    mtx_unlock(&self->mutex);
}

static
int get_codepoint_advance(fontrdr_t *self, uint32_t code)
{
    for(unsigned i = 0; i < self->fontnum; ++i)
    {
        int advance = fontrdr_file_get_advance(self->fontlist[i], code);
        if(advance)
            return advance;
    }

    return 0;
}

static
int set_font_size(fontrdr_t *self, unsigned size)
{
    if( size && size == self->height )
        return 0;
    if(!self->fontnum)
        return ENOENT;

    self->height = size;
    for(unsigned i = 0; i < self->fontnum; ++i)
    {
        fontrdr_file_t *font = self->fontlist[i];

        if( fontrdr_file_get_size(font) != size )
            fontrdr_file_set_size(font, size);

        int height = fontrdr_file_get_size(font);
        if( self->height < height )
            self->height = height;
    }

    self->space_width = 1;
    int advance = get_codepoint_advance(self, 0x20);
    if( self->space_width < advance )
        self->space_width = advance;

    self->ascii_width = self->space_width;
    for(uint32_t code = 0x21; code < 0x7F; ++code)
    {
        int advance = get_codepoint_advance(self, code);
        if( self->ascii_width < advance )
            self->ascii_width = advance;
    }

    return 0;
}

static
bool is_printable(uint32_t code)
{
    return ( 0x20 <= code && code < 0x7F ) ||
        ( 0x80 <= code && ( code < 0xD800 || 0xE000 <= code ) );
}

static
unsigned ceil_width(unsigned x, unsigned base)
{
    return ( x + base - 1 ) / base * base;
}

static
greyimg_t* gen_glyph_from_fonts(
    fontrdr_file_t **fontlist,
    unsigned fontnum,
    uint32_t code,
    unsigned size,
    int flags,
    int *uline_y,
    int *uline_d)
{
    greyimg_t *img = NULL;

    int style = flags & FONTRDR_STYLEMASK;

    for(unsigned i = 0; i < fontnum; ++i)
    {
        fontrdr_file_t *font = fontlist[i];

        if( style != fontrdr_file_get_style(font) )
            continue;

        if(( img = fontrdr_file_render(font, code) ))
        {
            *uline_y = fontrdr_file_get_uline_y(font);
            *uline_d = fontrdr_file_get_uline_thickness(font);
            break;
        }
    }

    return img;
}

static
greyimg_t* gen_square(int w, int h, uint32_t colour)
{
    greyimg_t *img = uimg_create(w, h);
    if(!img)
        return NULL;

    int len = MIN(w, h);
    int b = len / 8;
    int d = MAX(b, 1);

    uimg_begin_buf_access(img);

    for(int y = 0; y < h; ++y)
    {
        bool y_in_top_frame = b <= y && y < b + d;
        bool y_in_bottom_frame = h - b - d <= y && y < h - b;
        bool y_in_centre_area = b + d <= y && y < h - b - d;
        bool y_in_frame = y_in_top_frame || y_in_bottom_frame;

        uint32_t *row = uimg_get_row_buf(img, y);
        for(int x = 0; x < w; ++x)
        {
            bool x_in_left_frame = b <= x && x < b + d;
            bool x_in_right_frame = w - b - d <= x && x < w - b;
            bool x_in_centre_area = b + d <= x && x < w - b - d;
            bool x_in_frame = x_in_left_frame || x_in_right_frame;
            bool x_in_graph = x_in_frame || x_in_centre_area;

            row[x] =
                y_in_centre_area && x_in_frame ? 255 :
                y_in_frame && x_in_graph ? 255 :
                0;
        }
    }

    uimg_end_buf_access(img);

    return img;
}

static
void render_colour_from_grey(uimg_t *dst, const greyimg_t *src, uint32_t colour)
{
    int dst_w = uimg_get_width(dst);
    int dst_h = uimg_get_height(dst);
    int src_w = uimg_get_width(src);
    int src_h = uimg_get_height(src);
    int x_off = ( dst_w - src_w ) / 2;

    unsigned b = UIMG_COLOUR_GET_B(colour);
    unsigned g = UIMG_COLOUR_GET_G(colour);
    unsigned r = UIMG_COLOUR_GET_R(colour);
    unsigned a = UIMG_COLOUR_GET_A(colour);

    uimg_begin_buf_access(dst);
    uimg_begin_buf_access(src);

    for(int dst_y = 0; dst_y < dst_h; ++dst_y)
    {
        int src_y = dst_y;
        uint32_t *dst_row = uimg_get_row_buf(dst, dst_y);
        const uint32_t *src_row = uimg_get_row_buf_c(src, src_y);

        for(int dst_x = 0; dst_x < dst_w; ++dst_x)
        {
            int src_x = dst_x - x_off;
            int scale = src_y < src_h && 0 <= src_x && src_x < src_w ?
                src_row[src_x] : 0;
            dst_row[dst_x] = UIMG_MAKE_COLOUR(
                ( scale * b ) >> 8,
                ( scale * g ) >> 8,
                ( scale * r ) >> 8,
                ( scale * a ) >> 8);
        }
    }

    uimg_end_buf_access(dst);
    uimg_end_buf_access(src);
}

static
void render_underline(uimg_t *img, int y, int d, uint32_t colour)
{
    int w = uimg_get_width(img);

    uimg_begin_buf_access(img);

    for(int i = 0; i < d; ++i)
    {
        uint32_t *row = uimg_get_row_buf(img, y + i);
        for(int x = 0; x < w; ++x)
            row[x] = colour;
    }

    uimg_end_buf_access(img);
}

uimg_t* fontrdr_gen_glyph(
    fontrdr_t *self, uint32_t code, uint32_t colour, unsigned size, int flags)
{
    uimg_t *glyph = NULL;

    greyimg_t *greyimg = NULL;
    uimg_t *colourimg = NULL;

    mtx_lock(&self->mutex);

    do
    {
        if(set_font_size(self, size))
            break;

        // Force modify parameters if the specific code point is not printable
        bool printable = is_printable(code);
        if(!printable)
        {
            code = 0;
            flags = ( flags & FONTRDR_MONOSPACE ) ?
                FONTRDR_MONOSPACE : FONTRDR_REGULAR;
        }

        // Try to use the already existed glyph from cache
        const struct fontrdr_cnode *info =
            fontrdr_cache_find(&self->cache, code, colour, size, flags);
        if(info)
        {
            glyph = uimg_shadow(info->img);
            break;
        }

        // Try generate glyph from font modules
        int uline_y = 0, uline_d = 0;
        if( !greyimg && printable )
        {
            greyimg = gen_glyph_from_fonts(
                self->fontlist, self->fontnum, code, self->height, flags,
                &uline_y, &uline_d);
        }

        // Fallback regular style then try generate glyph again
        if( !greyimg && printable && ( flags & FONTRDR_STYLEMASK ) )
        {
            int fallback_flags = flags & ~FONTRDR_STYLEMASK;
            greyimg = gen_glyph_from_fonts(
                self->fontlist, self->fontnum, code, self->height, fallback_flags,
                &uline_y, &uline_d);
        }

        // Generate the replacement glyph if no any glyph be generated
        bool underlined = flags & FONTRDR_UNDERLINE;
        if(!greyimg)
        {
            underlined = false;
            if(!( greyimg = gen_square(self->space_width, self->height, colour) ))
                break;
        }

        // Prepare a canvas for colour image
        bool monospaced = flags & FONTRDR_MONOSPACE;
        int width = uimg_get_width(greyimg);
        if(monospaced)
            width = ceil_width(width, self->ascii_width);

        // Render colour image from grey scale glyph
        if(!( colourimg = uimg_create(width, self->height) ))
            break;
        render_colour_from_grey(colourimg, greyimg, colour);

        // Render the underline if requested
        if(underlined)
            render_underline(colourimg, uline_y, uline_d, colour);

        // Add the new generated colour image to the glyph cache
        fontrdr_cache_add(&self->cache, code, colour, size, flags, colourimg);

        glyph = uimg_shadow(colourimg);
    } while(false);

    mtx_unlock(&self->mutex);

    if(colourimg)
        uimg_release(colourimg);
    if(greyimg)
        uimg_release(greyimg);

    return glyph;
}
