#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "flags.h"
#include "fontrdr_cache.h"

#ifdef __GNUC__
#   pragma GCC push_options
#   pragma GCC optimize("no-strict-aliasing")
#endif
RB_GENERATE(fontrdr_cmap, fontrdr_cnode, map_node, fontrdr_cnode_compare);
#ifdef __GNUC__
#   pragma GCC pop_options
#endif

int fontrdr_cnode_compare(
    const struct fontrdr_cnode *l, const struct fontrdr_cnode *r)
{
    int code_diff = (int) l->code - (int) r->code;
    int colour_diff = (long) l->colour - (long) r->colour;
    int flags_diff = (int) l->size_flags - (int) r->size_flags;
    return code_diff ? code_diff :
        colour_diff ? colour_diff :
        flags_diff;
}

void fontrdr_cache_init(fontrdr_cache_t *self)
{
    self->node_capa = 0;
    self->node_num = 0;
    self->node_pool = NULL;

    INIT_LIST_HEAD(&self->free_list);
    INIT_LIST_HEAD(&self->using_list);
    RB_INIT(&self->glyph_map);
}

void fontrdr_cache_destroy(fontrdr_cache_t *self)
{
    fontrdr_cache_dealloc(self);
}

int fontrdr_cache_alloc(fontrdr_cache_t *self, unsigned num)
{
    if( num == self->node_num )
        return 0;

    if( num < self->node_num )
    {
        self->node_num = num;
        fontrdr_cache_clear(self);
        return 0;
    }

    if( num > self->node_capa )
    {
        unsigned new_capa = num;
        size_t new_size = new_capa * sizeof(self->node_pool[0]);
        struct fontrdr_cnode *new_arr = realloc(self->node_pool, new_size);
        if(!new_arr)
            return ENOMEM;

        for(unsigned i = self->node_capa; i < new_capa; ++i)
            memset(&new_arr[i], 0, sizeof(new_arr[i]));
        self->node_capa = new_capa;
        self->node_pool = new_arr;
    }

    for(unsigned i = self->node_num; i < num; ++i)
    {
        struct fontrdr_cnode *node = &self->node_pool[i];
        list_add_tail(&node->list_node, &self->free_list);
    }

    self->node_num = num;

    return 0;
}

void fontrdr_cache_dealloc(fontrdr_cache_t *self)
{
    if(self->node_pool)
    {
        for(unsigned i = 0; i < self->node_num; ++i)
        {
            struct fontrdr_cnode *node = &self->node_pool[i];
            if(node->img)
                uimg_release(node->img);
        }
        free(self->node_pool);
    }

    self->node_capa = 0;
    self->node_num = 0;
    self->node_pool = NULL;

    INIT_LIST_HEAD(&self->free_list);
    INIT_LIST_HEAD(&self->using_list);
    RB_INIT(&self->glyph_map);
}

static
struct fontrdr_cnode* extract_free_node(fontrdr_cache_t *self)
{
    struct fontrdr_cnode *node = list_first_entry_or_null(
        &self->free_list, struct fontrdr_cnode, list_node);
    if(node)
    {
        list_del(&node->list_node);
        return node;
    }

    node = list_first_entry_or_null(
        &self->using_list, struct fontrdr_cnode, list_node);
    if(!node)
        return NULL;

    list_del(&node->list_node);
    RB_REMOVE(fontrdr_cmap, &self->glyph_map, node);

    if(node->img)
    {
        uimg_release(node->img);
        node->img = NULL;
    }

    return node;
}

const struct fontrdr_cnode* fontrdr_cache_add(
    fontrdr_cache_t *self,
    uint32_t code,
    uint32_t colour,
    unsigned size,
    int flags,
    const uimg_t *img)
{
    if(!img)
        return NULL;

    struct fontrdr_cnode *node = extract_free_node(self);
    if(!node)
        return NULL;

    assert(!node->img);
    node->code = code;
    node->colour = colour;
    node->size_flags = ( flags & FONTRDR_FLAGSMASK ) | ( size & FONTRDR_SIZEMASK );
    node->img = uimg_shadow(img);

    struct fontrdr_cnode *duplicated =
        RB_INSERT(fontrdr_cmap, &self->glyph_map, node);
    if(duplicated)
    {
        uimg_release(node->img);
        node->img = NULL;

        list_add_tail(&node->list_node, &self->free_list);
        return NULL;
    }

    list_add_tail(&node->list_node, &self->using_list);
    return 0;
}

const struct fontrdr_cnode* fontrdr_cache_find(
    fontrdr_cache_t *self,
    uint32_t code,
    uint32_t colour,
    unsigned size,
    int flags)
{
    struct fontrdr_cnode key =
    {
        .code = code,
        .colour = colour,
        .size_flags = ( flags & FONTRDR_FLAGSMASK ) | ( size & FONTRDR_SIZEMASK ),
    };

    return RB_FIND(fontrdr_cmap, &self->glyph_map, &key);
}

void fontrdr_cache_clear(fontrdr_cache_t *self)
{
    INIT_LIST_HEAD(&self->free_list);
    INIT_LIST_HEAD(&self->using_list);
    RB_INIT(&self->glyph_map);

    for(unsigned i = 0; i < self->node_num; ++i)
    {
        struct fontrdr_cnode *node = &self->node_pool[i];

        list_add_tail(&node->list_node, &self->free_list);
        if(node->img)
        {
            uimg_release(node->img);
            node->img = NULL;
        }
    }
}
