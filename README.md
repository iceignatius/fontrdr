# Font Render Library

A font renderer to render single character to an image.

This library is designed against my projects needs,
and may not be so useful for every one.
I create this renderer library for some purpose:

* Hide massive font operations and details that may not be too important for me
* Encapsulate and unite operations between several font render implementations
* Generate uniform character images whiches have mostly same geometry parameters
    to simplify the implementations whiches use this renderer
* Let other text renderers to share the same character renderer and fonts and cache
