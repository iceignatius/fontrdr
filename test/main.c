#include <stddef.h>
#include <stdbool.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>
#include "ctest_cmp_img.h"
#include "fontrdr.h"

#ifdef UIMG_USE_SDL
#   include <SDL2/SDL.h>
#endif

#define TEST_FONT_SIZE      64
#define TEST_FONT_COLOUR    UIMG_MAKE_COLOUR(128, 255, 64, 255)
#define TEST_CACHE_NUM      128

/*
 * Set a suitable (but smallest as possible) image comparison value to
 * avoid the sensitivity problem between different font renderer implementations
 */
#define TEST_COMP_MAXDIFF   0

static const uint32_t ascii_sample_list[] =
{
    0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F,
    0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F,
    0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F,
    0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x5B, 0x5C, 0x5D, 0x5E, 0x5F,
    0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F,
    0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E,
};

static const unsigned ascii_sample_num =
    sizeof(ascii_sample_list)/sizeof(ascii_sample_list[0]);

static const uint32_t european_sample_list[] =
{
    0xC0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC8, 0xC9, 0xCA, 0xCB, 0xCC, 0xCD, 0xCE, 0xCF,
    0xD0, 0xD1, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6, 0xD7, 0xD8, 0xD9, 0xDA, 0xDB, 0xDC, 0xDD, 0xDE, 0xDF,
};

static const unsigned european_sample_num =
    sizeof(european_sample_list)/sizeof(european_sample_list[0]);

static const uint32_t chinese_sample_list[] =
{
    // 文 字 之 美
    0x6587, 0x5B57, 0x4E4B, 0x7F8E
};

static const unsigned chinese_sample_num =
    sizeof(chinese_sample_list)/sizeof(chinese_sample_list[0]);

static
int setup_test_res(void **state)
{
#ifdef UIMG_USE_SDL
    int err;
    if(( err = SDL_Init(SDL_INIT_VIDEO) ))
        return err;
#endif

    assert_int_equal(system("test -d dump || mkdir dump"), 0);

    fontrdr_t *rdr = fontrdr_create();
    assert_non_null(rdr);

    assert_int_equal(fontrdr_set_cache_num(rdr, TEST_CACHE_NUM), 0);

    *state = rdr;
    return 0;
}

static
int teardown_test_res(void **state)
{
    fontrdr_t *rdr = *state;
    fontrdr_release(rdr);

#ifdef UIMG_USE_SDL
    SDL_Quit();
#endif

    return 0;
}

static
int test_render_glyphs(
    fontrdr_t *rdr,
    const char *label,
    int flags,
    const uint32_t *codelist,
    unsigned codenum)
{
    bool bolded = flags & FONTRDR_BOLD;
    bool obliquely = flags & FONTRDR_OBLIQUE;
    bool monowid = flags & FONTRDR_MONOSPACE;
    bool underlined = flags & FONTRDR_UNDERLINE;

    for(unsigned i = 0; i < codenum; ++i)
    {
        uint32_t code = codelist[i];

        char case_name[64];
        snprintf(case_name, sizeof(case_name), "glyph-%s-%s%s%s%s%s-%05u(%02X).png",
            label,
            bolded ? "b" : "",
            obliquely ? "o" : "",
            monowid ? "m" : "",
            underlined ? "u" : "",
            !bolded && !obliquely && !monowid && !underlined ? "r" : "",
            code,
            code);

        char sample_name[128], dump_name[128];
        snprintf(sample_name, sizeof(sample_name), "samples/%s", case_name);
        snprintf(dump_name, sizeof(dump_name), "dump/%s", case_name);

        uimg_t *img = fontrdr_gen_glyph(
            rdr, code, TEST_FONT_COLOUR, TEST_FONT_SIZE, flags);
        if(!img)
            return code;

        long diff = load_compare_image(sample_name, img);
        uimg_release(img);

        if( diff < 0 || TEST_COMP_MAXDIFF < diff )
        {
            uimgfile_save_file(img, dump_name, 0);
            return code;
        }
    }

    return 0;
}

static
void load_liberation_fonts(void **state)
{
    fontrdr_t *rdr = *state;

    fontrdr_clear_fonts(rdr);
    assert_int_equal(fontrdr_add_fonts(rdr, "fonts/LiberationSans-*.ttf"), 0);
}

static
void test_ascii_of_liberation_regular(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "liberation-ascii", FONTRDR_REGULAR,
        ascii_sample_list, ascii_sample_num));
}

static
void test_ascii_of_liberation_bold(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "liberation-ascii", FONTRDR_BOLD,
        ascii_sample_list, ascii_sample_num));
}

static
void test_ascii_of_liberation_oblique(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "liberation-ascii", FONTRDR_OBLIQUE,
        ascii_sample_list, ascii_sample_num));
}

static
void test_ascii_of_liberation_bold_oblique(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "liberation-ascii", FONTRDR_BOLD | FONTRDR_OBLIQUE,
        ascii_sample_list, ascii_sample_num));
}

static
void test_ascii_of_liberation_monospace(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "liberation-ascii", FONTRDR_MONOSPACE,
        ascii_sample_list, ascii_sample_num));
}

static
void test_ascii_of_liberation_underline(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "liberation-ascii", FONTRDR_UNDERLINE,
        ascii_sample_list, ascii_sample_num));
}

static
void test_ascii_of_liberation_monospace_underline(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "liberation-ascii", FONTRDR_MONOSPACE | FONTRDR_UNDERLINE,
        ascii_sample_list, ascii_sample_num));
}

static
void test_european_of_liberation_regular(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "liberation-european", FONTRDR_REGULAR,
        european_sample_list, european_sample_num));
}

static
void test_european_of_liberation_monospace(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "liberation-european", FONTRDR_MONOSPACE,
        european_sample_list, european_sample_num));
}

static
void test_european_of_liberation_underline(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "liberation-european", FONTRDR_UNDERLINE,
        european_sample_list, european_sample_num));
}

static
void load_taipei_fonts(void **state)
{
    fontrdr_t *rdr = *state;

    fontrdr_clear_fonts(rdr);
    assert_int_equal(fontrdr_add_fonts(rdr, "fonts/TaipeiSansTCBeta-*.ttf"), 0);
}

static
void test_ascii_of_taipei_regular(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "taipei-ascii", FONTRDR_REGULAR,
        ascii_sample_list, ascii_sample_num));
}

static
void test_ascii_of_taipei_bold(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "taipei-ascii", FONTRDR_BOLD,
        ascii_sample_list, ascii_sample_num));
}

static
void test_ascii_of_taipei_monospace(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "taipei-ascii", FONTRDR_MONOSPACE,
        ascii_sample_list, ascii_sample_num));
}

static
void test_ascii_of_taipei_underline(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "taipei-ascii", FONTRDR_UNDERLINE,
        ascii_sample_list, ascii_sample_num));
}

static
void test_european_of_taipei_regular(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "taipei-european", FONTRDR_REGULAR,
        european_sample_list, european_sample_num));
}

static
void test_european_of_taipei_monospace(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "taipei-european", FONTRDR_MONOSPACE,
        european_sample_list, european_sample_num));
}

static
void test_european_of_taipei_underline(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "taipei-european", FONTRDR_UNDERLINE,
        european_sample_list, european_sample_num));
}

static
void test_chinese_of_taipei_regular(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "taipei-chinese", FONTRDR_REGULAR,
        chinese_sample_list, chinese_sample_num));
}

static
void test_chinese_of_taipei_monospace(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "taipei-chinese", FONTRDR_MONOSPACE,
        chinese_sample_list, chinese_sample_num));
}

static
void test_chinese_of_taipei_underline(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "taipei-chinese", FONTRDR_UNDERLINE,
        chinese_sample_list, chinese_sample_num));
}

static
void load_twkai_fonts(void **state)
{
    fontrdr_t *rdr = *state;

    fontrdr_clear_fonts(rdr);
    assert_int_equal(fontrdr_add_fonts(rdr, "fonts/TW-Kai-98_1.ttf"), 0);
}

static
void test_ascii_of_twkai_regular(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "twkai-ascii", FONTRDR_REGULAR,
        ascii_sample_list, ascii_sample_num));
}

static
void test_ascii_of_twkai_monospace(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "twkai-ascii", FONTRDR_MONOSPACE,
        ascii_sample_list, ascii_sample_num));
}

static
void test_ascii_of_twkai_underline(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "twkai-ascii", FONTRDR_UNDERLINE,
        ascii_sample_list, ascii_sample_num));
}

static
void test_european_of_twkai_regular(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "twkai-european", FONTRDR_REGULAR,
        european_sample_list, european_sample_num));
}

static
void test_european_of_twkai_monospace(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "twkai-european", FONTRDR_MONOSPACE,
        european_sample_list, european_sample_num));
}

static
void test_european_of_twkai_underline(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "twkai-european", FONTRDR_UNDERLINE,
        european_sample_list, european_sample_num));
}

static
void test_chinese_of_twkai_regular(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "twkai-chinese", FONTRDR_REGULAR,
        chinese_sample_list, chinese_sample_num));
}

static
void test_chinese_of_twkai_monospace(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "twkai-chinese", FONTRDR_MONOSPACE,
        chinese_sample_list, chinese_sample_num));
}

static
void test_chinese_of_twkai_underline(void **state)
{
    fontrdr_t *rdr = *state;

    assert_int_equal(0, test_render_glyphs(
        rdr, "twkai-chinese", FONTRDR_UNDERLINE,
        chinese_sample_list, chinese_sample_num));
}

int main(int argc, char *argv[])
{
    struct CMUnitTest tests[] =
    {
        cmocka_unit_test(load_liberation_fonts),
        cmocka_unit_test(test_ascii_of_liberation_regular),
        cmocka_unit_test(test_ascii_of_liberation_bold),
        cmocka_unit_test(test_ascii_of_liberation_oblique),
        cmocka_unit_test(test_ascii_of_liberation_bold_oblique),
        cmocka_unit_test(test_ascii_of_liberation_monospace),
        cmocka_unit_test(test_ascii_of_liberation_underline),
        cmocka_unit_test(test_ascii_of_liberation_monospace_underline),
        cmocka_unit_test(test_european_of_liberation_regular),
        cmocka_unit_test(test_european_of_liberation_monospace),
        cmocka_unit_test(test_european_of_liberation_underline),

        cmocka_unit_test(load_taipei_fonts),
        cmocka_unit_test(test_ascii_of_taipei_regular),
        cmocka_unit_test(test_ascii_of_taipei_bold),
        cmocka_unit_test(test_ascii_of_taipei_monospace),
        cmocka_unit_test(test_ascii_of_taipei_underline),
        cmocka_unit_test(test_european_of_taipei_regular),
        cmocka_unit_test(test_european_of_taipei_monospace),
        cmocka_unit_test(test_european_of_taipei_underline),
        cmocka_unit_test(test_chinese_of_taipei_regular),
        cmocka_unit_test(test_chinese_of_taipei_monospace),
        cmocka_unit_test(test_chinese_of_taipei_underline),

        cmocka_unit_test(load_twkai_fonts),
        cmocka_unit_test(test_ascii_of_twkai_regular),
        cmocka_unit_test(test_ascii_of_twkai_monospace),
        cmocka_unit_test(test_ascii_of_twkai_underline),
        cmocka_unit_test(test_european_of_twkai_regular),
        cmocka_unit_test(test_european_of_twkai_monospace),
        cmocka_unit_test(test_european_of_twkai_underline),
        cmocka_unit_test(test_chinese_of_twkai_regular),
        cmocka_unit_test(test_chinese_of_twkai_monospace),
        cmocka_unit_test(test_chinese_of_twkai_underline),
    };

    return cmocka_run_group_tests_name(
        "Font Renderer Test", tests, setup_test_res, teardown_test_res);
}
