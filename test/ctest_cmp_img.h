#ifndef _CTEST_COMPARE_IMAGE_H_
#define _CTEST_COMPARE_IMAGE_H_

#include <limits.h>
#include <stdlib.h>
#include <uimg/uimgfile.h>

static
long load_compare_image(const char *filename, const uimg_t *t_img)
{
    /*
     * Return:
     * + Positive (including zero): Average value difference per pixel
     * + Negative: Error occurred or properties mismatch
     */
    uimg_t *f_img = NULL;

    long err = -1;
    do
    {
        if( !( f_img = uimg_create(0, 0) ) )
        {
            err = -3;
            break;
        }

        if(uimgfile_load_file(f_img, filename, 0))
        {
            err = -2;
            break;
        }

        if( uimg_get_width(f_img) != uimg_get_width(t_img) ||
            uimg_get_height(f_img) != uimg_get_height(t_img) )
        {
            err = -1;
            break;
        }

        uimg_begin_buf_access(f_img);
        uimg_begin_buf_access(t_img);

        long long diff = 0;
        int w = uimg_get_width(t_img);
        int h = uimg_get_height(t_img);
        for(int y = 0; y < h; ++y)
        {
            const uint32_t *f_row = uimg_get_row_buf_c(f_img, y);
            const uint32_t *t_row = uimg_get_row_buf_c(t_img, y);
            if( !f_row || !t_row )
            {
                diff = LLONG_MAX;
                break;
            }

            for(int x = 0; x < w; ++x)
            {
                diff += abs( (int) UIMG_COLOUR_GET_B(f_row[x]) - (int) UIMG_COLOUR_GET_B(t_row[x]) );
                diff += abs( (int) UIMG_COLOUR_GET_G(f_row[x]) - (int) UIMG_COLOUR_GET_G(t_row[x]) );
                diff += abs( (int) UIMG_COLOUR_GET_R(f_row[x]) - (int) UIMG_COLOUR_GET_R(t_row[x]) );
                diff += abs( (int) UIMG_COLOUR_GET_A(f_row[x]) - (int) UIMG_COLOUR_GET_A(t_row[x]) );
            }
        }

        uimg_end_buf_access(t_img);
        uimg_end_buf_access(f_img);

        diff /= ( w * h * 4 );
        err = diff;

    } while(0);

    if(f_img)
        uimg_release(f_img);

    return err;
}

#endif
